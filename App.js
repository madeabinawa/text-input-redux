/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import InputRedux from './components/InputRedux';

const App = () => {
  const initState = { output: '' };

  function reducer(state = initState, action) {
    switch (action.type) {
      case 'set_output':
        return { output: action.value };
    }
    return state;
  }

  const store = createStore(reducer);

  return (
    <Provider store={store}>
      <View style={styles.container}>
        <InputRedux />
      </View>
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d9d8f8',
    padding: 8,
  },
});
