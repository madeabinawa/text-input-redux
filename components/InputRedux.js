/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { connect } from 'react-redux';

const InputRedux = (props) => {
    return (
        <View>
            <Text style={styles.paragraph}>
                Masukkan kalimat pada kontak di bawah ini.
      </Text>
            <TextInput style={styles.input} onChangeText={props.handlerOnChangeText} multiline={true} numberOfLines={5} />
            <Text style={styles.paragraph}>
                Outputs:{'\n'} {props.output}
            </Text>
            <Button title="Munculkan Pesan" onPress={() => alert(props.output)} />
        </View>
    );
};

function mapStateToProps(state) {
    return { output: state.output };
}
function mapDispatchToProps(dispatch) {
    return {
        handlerOnChangeText: (value) => dispatch({ type: 'set_output', value }),
    };
}



const styles = StyleSheet.create({
    paragraph: {
        margin: 24,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    input: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        height: 100,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(InputRedux);
